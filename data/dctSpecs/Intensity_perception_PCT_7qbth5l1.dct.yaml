dct:
  version: 0.2.6
  id: Intensity_perception_PCT_7qbth5l1
  label: Intensity perception
  date: '2023-09-05'
  dct_version: '1'
  ancestry: ''
  retires: ''
  definition:
    definition: 'Intensity perceptions are at the lowest level of the perceptual hierarchy,
      controlling sensory information. An input signal at this level is some function
      of a physical effect on a nerve ending. This consists of neural currents of
      certain rate of firing, that goes along neural paths to so-called ''loops''
      at higher levels (Runkel, 2003). It is the magnitude of the input, regardless
      of the modality of the experience. The speed of firing of the neural paths depends
      on external forces and sources, but they are not ''pictures'' of external reality.
      It is only an analogous translation (within the organism) of the energy coming
      from the external world (Runkel, 2003). For example: Light of a certain intensity
      hits our retina, but our eyes can only detect a small fraction of the light
      spectrum, and it can also only detect light that comes from the front (Runkel,
      2003). The intensity perception refers to the perceived input, not to the outside
      environment. Sense receptors only transmit a particular sort of energy. For
      instance, a pressure receptor only transmits signals of pressure, not when heated
      or when light hits it (although all nerves can be stimulated by electrical or
      chemical stimulation). When pressure activates a pressure receptor, it tells
      us nothing about the cause of the pressure. Intensities are merely the input
      signal from a nerve ending (Runkel, 2003). '
  measure_dev:
    instruction: 'To measure intensities, first it needs to be determined what type
      of perception is going to be measured. The participant needs to be exposed to
      some kind of ‘stimulus’ that can be perceived at the neural level. To measure
      intensities, one would need to measure physiological variables on the level
      of a single neuron. '
  measure_code:
    instruction: 'An instrument measures intensities when it measures the intensity
      (e.g. firing rate) of a single neuron.  '
  aspect_dev:
    instruction: 'Intensity perceptions, in the present definition, cannot be studied
      with qualitative research. '
  aspect_code:
    instruction: 'Intensity perceptions, in the present definition, cannot be studied
      with qualitative research. '
  comments: ''
  rel: ~


