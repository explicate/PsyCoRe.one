dct:
  version: 0.2.6
  id: ProblematicHypersexuality_7qfdty7g
  label: Problematic Hypersexuality 2.0
  date: '2023-09-12'
  dct_version: '1'
  ancestry: ''
  retires: ''
  definition:
    definition: "Problematic Hypersexuality (PH) is succinctly defined as the experience
      of distress and negative consequences due to hypersexual urges and behavior
      - to the extent that the experience of PH causes the individual to at least
      consider seeking help. We have adapted the previous extended definition of PH
      ( https://psycore.one/problematicHypersexuality_7mm4hr4f ) in accordance with
      the results of our validation study: https://open-research-europe.ec.europa.eu/articles/3-129/v1
      (preprint). The previous definition was based on three perspectives on PH: 1)
      Sex addiction; 2) Hypersexual Disorder; and 3) Compulsive Sexual Behavior Disorder.
      The current definition makes first and foremost use of the outcomes of the validation
      study.\nBefore we present the extended definition, we note that our definition
      of PH must be taken as a construct definition for research, not as a (precursor
      of a) diagnosis. This means, among others, that what are called ‘criteria’ within
      available diagnostic systems for PH, we regard as ‘characteristics’ of PH in
      our definition. With this Decentralized Construct Taxonomy (DCT) definition
      of PH we hope to delineate as clearly as possible what we mean by PH in our
      studies. As research in the field progresses, the DCT-definition of PH will
      change to stay in line with the results of research. We also need to mention
      that others researchers might define other but similar constructs for the phenomena
      we try to define. If these other definitions are detailed and explicit, as are
      our own, this will produce heterogeneity but also clarity in used definitions.
      Current research practices often suffer from hidden heterogeneity: definitions
      are not made explicit or are imprecise and lack sufficient detail. Often constructs
      that bear the same name, prove to differ in small but important details. Reasons
      to adhere to explicit heterogeneity in definitions of constructs are explained
      in more detail here: (Peters & Crutzen, 2022, https://doi.org/10.31234/osf.io/8tpcv).\n\nIn
      the validation study we performed, we tested ten characteristics of PH and found
      that three were lacking in cue validity. With cue validity we mean the extent
      to which a characteristic is unique to a certain category (Rosch & Mervis, 1975)
      – PH in this case. The characteristics of Tolerance and Withdrawal symptoms
      did not sufficiently discriminate between a PH and a Non-problematic Hypersexual
      (NH) sample. Therefore, these characteristics are not included in the current
      definition. We do note, however, that despite the lack of cue validity, these
      two characteristics warrant further investigation – current results might be
      due to phrasing or other item content related issues. The characteristic of
      “Intense focus on and preoccupation with sex” also proved to be lacking in cue
      validity. This characteristic does not warrant further testing as it seems clear
      that it will not be functional in discriminating between PH and NH. Preoccupation
      with sex we see a necessary condition for PH but it cannot be considered as
      unique to PH, as our validation study showed. Furthermore, a new characteristic
      can be added to the list, based on our study, namely that of “Emotion dysregulation
      related to increases in sexual desire”. Previously this characteristic was only
      seen as a driver of PH, and not as a stable trait measurable with one-off instruments.
      Our study showed that “Emotion dysregulation” measured cross-sectionally has
      sufficient cue validity to distinguish between PH and NH. Thus, we propose to
      regard this characteristic as a stable unique trait of PH. \n\nWe now turn to
      the extended DCT-definition of PH. We specify eight stable trait characteristics:
      1) Repetitive sexual activities leading to neglect of selfcare and responsibilities;
      2) Negative consequences in one or more life domains; 3) Disregard of risk of
      harm to self or others; 4) Failed attempts to stop the sexual behavior; 5) Continue
      PH behavior despite negative consequences; 6) Sex used to cope with dysphoric
      mood states or stressful life events; 7) Continue PH behavior despite loss of
      pleasure in sex; and 8) Emotion dysregulation related to increased preoccupation
      with sex. These eight cue valid characteristics of PH we consider as relatively
      stable. Furthermore, we consider PH to be ruled by two drivers: 1) high sexual
      desire; and 2) emotion dysregulation related to sexual desire (Kingston, 2018;
      specifies these drivers as ‘dimensions’). These two drivers we see as necessary
      for PH to occur but not as sufficient for PH to be the case. The eight stable
      characteristics are connected to the two drivers as outcomes are connected to
      processes. The process in PH that is maintained by the two drivers is that of
      a vicious circle – we hypothesize. Negative feelings (e.g. shame or low self-esteem)
      lead to sexually acting out in order to cope with them. The sexual acting out
      reinforces shame, thus increasing the risk of a new cycle of shame and sexually
      acting out. Our investigations of this vicious circle are underway but are at
      the time not conclusive. Also therefore, this DCT of PH should be considered
      as preliminary and open to change. If a vicious circle for PH is not confirmed
      - or even disconfirmed - this will lead to a different DCT for PH. If a vicious
      circle is confirmed, this might lead to a more detailed description of its nature,
      and this too will lead to an adaptation of this DCT for PH. \n\nWe offer the
      following figure as an overview of this DCT definition of PH and note that cue
      valid characteristics are best established using IRT validation, while measuring
      the driving processes of PH can best be done using ESM techniques: https://osf.io/b5hz3
      \  Both research methodologies (IRT and ESM) will profit from comparing a PH
      subpopulation with relevant other subpopulations and we suggest to consider
      an NH population for this purpose, but other subpopulations might be considered
      as well. This has been elaborated on in the article “Three quarks for hypersexuality
      research – van Tuijl et al., 2023a ” https://doi.org/10.3390/sexes4010011 \n\nOn
      a final note, we mention two venues for future studies. These can be added to
      the main theme of future research: – Is there a vicious circle of emotion dysregulation
      in PH? And if there is, how does it work? The two added venues are:\n1) New
      DCT’s for NH and Hypersexuality might be developed based on further research
      and development. While NH in short might be defined as the experience of intense
      involvement in sexuality without distress, hypersexuality might in short be
      defined as intense involvement in sexuality. Extended definitions of NH and
      Hypersexuality should be based on a back and forth between theory and data.\n2)
      The connection between drivers of PH and the stable traits of PH warrants further
      research. Associations between the stable traits and fluctuating sexual desire
      and emotion (dys)regulation can be investigated with multilevel models. Currently
      experience sampling datacollection is being prepared that will allow for the
      investigation of associations between person level traits of PH and fluctuating
      sexual desire and mood states. Especially the associations between the stable
      trait of “Emotion dysregulation related to increased preoccupation with sex”
      (e.g. “start thinking about sex when down” or “having sex to escape feelings
      of despair”) and the fluctuations of “Emotion dysregulation related to sexual
      desire” (one of the two drivers in our preliminary definition) needs clarification.
      \n"
  measure_dev:
    instruction: |-
      Measurement instruments can be developed for one-off and dynamical assessment:

      One-off measurement instruments – In a new validation study several previously developed measurement instruments have been gathered and their items investigated on their psychometric merits and their content and cue validity (van Tuijl et al., 2023b – preprint). With cue validity we mean the extent to which an item expresses a unique characteristic of PH that is not – or less – extant in other subpopulations. Item Response Theorie (IRT) techniques have been used together with Patient Routine Outcome Monitoring Information System (PROMIS, Prinsen et al., 2018) methods to validate two factors: Emotion Dysregulation – PH (9 items) and Negative Effects – PH (17 items). As is noted in the validation study, there still is overlap between the PH and NH subpopulations. This means that a search for new items – reflecting other characteristics – should continue and the current scales adapted if new research provides reason for this.

      Dynamical assessment – As specified in the definition, we see PH as driven by high sexual desire in combination with emotion dysregulation. These dynamical and fluctuating processes involved in PH we see as underlying the more permanent outcomes (each of the eight characteristics of PH). Emotion Dysregulation proves to be measurable at the trait level as well, as the validation study showed. Nonetheless, we think that Experience Sampling Method (ESM) techniques are most optimal to assess fluctuating processes that picture associations between emotions and sexuality in PH. ESM is an ecologically valid method that is capable to assess fluctuations in sexual desire and emotions and link these to sexual activity in temporal analyses. Currently, a number of items are involved in measuring sexual desire and emotions with ESM techniques. Response Process Evaluations (Wolf et al., 2019) of these items needs to be undertaken still.
  measure_code:
    instruction: For literature reviews no instructions for coding measurement instruments
      are given as this construct definition of PH is new. It might be investigated
      in a literature review as either sexual addiction, hypersexual disorder or compulsive
      sexual behavior disorder. All three perspectives shed light on a particular
      aspect of PH. Therefore, at this moment, a literature review on PH should rather
      consider the three perspectives on PH separately or in combination, to investigate
      previous studies on subjects related to PH.
  aspect_dev:
    instruction: |
      For different subpopulations instructions to elicit content will be different. In a qualitative study on chemsex participation, currently being developed, we will elicit construct content using the following coding structure https://osf.io/s843n
      We note that the vicious circle hypothesized for PH in general - as described in this DCT definition for PH - is an integral part of the coding structure. For other groups (e.g. NH and PH pornusers), a similar coding structure for PH (presented in the blue block in the coding structure) will be used, also including a vicious circle. However, the overall structure will be different, as pornuse and chemsex generally take place in very different settings (and this setting or context is part of the coding structure as well, as is exemplified in the a priori coding structure for the chemsex study).
  aspect_code:
    instruction: Construct content will be coded at first as separate themes, following
      the a priori coding structure for PH chemsex participation (in case of the chemsex
      study). This coding scheme already provides a potential network structure in
      which certain aspects of chemsex experiences can be more prominently linked
      to characteristics of PH than others. Such connections will be subsumed under
      new themes by axial coding. Then, attention can be given to the more prominently
      visible and nuanced aspects by assigning childcodes to them. Other coding techniques
      are possible as well.
  comments: |
    References:

    Kingston D. A. (2018). Hypersexuality: Fact or Fiction?. The journal of sexual medicine, 15(5), 613–615. https://doi.org/10.1016/j.jsxm.2018.02.015

    Peters, G. Y., & Crutzen, R. (2022, November 22). Knowing What We’re Talking About: Facilitating Decentralized, Unequivocal Publication of and Reference to Psychological Construct Definitions and Instructions. https://doi.org/10.31234/osf.io/8tpcv

    Prinsen, C. A. C., Mokkink, L. B., Bouter, L. M., Alonso, J., Patrick, D. L., de Vet, H. C. W., & Terwee, C. B. (2018). COSMIN guideline for systematic reviews of patient-reported outcome measures. Quality of life research : an international journal of quality of life aspects of treatment, care and rehabilitation, 27(5), 1147–1157. https://doi.org/10.1007/s11136-018-1798-3

    Rosch, E., & Mervis, C. B. (1975). Family resemblances: Studies in the internal structure of categories. Cognitive Psychology, 7(4), 573–605. https://doi.org/10.1016/0010-0285(75)90024-9

    van Tuijl, P., Verboon, P., & van Lankveld, J. (2023a). Three quarks for hypersexuality research. Sexes 4(1), 118-132. https://doi.org/10.3390/sexes4010011

    van Tuijl P., Verboon, P., & van Lankveld, J. (2023b). Initial development and validation of item banks to measure problematic hypersexuality [version 1; peer review: awaiting peer review]. Open Research Europe 3(129) (https://doi.org/10.12688/openreseurope.16131.1)

    Wolf, M. G., Ihm, E. D., Maul, A., & Taves, A. (2019, July 23). Survey Item Validation. https://doi.org/10.31234/osf.io/k27w3
  rel: ~


