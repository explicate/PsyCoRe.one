dct:
  version: 0.2.6
  id: Systemconcept_perception_PCT_7qbtf9r7
  label: System concept perception
  date: '2023-09-05'
  dct_version: '1'
  ancestry: ''
  retires: ''
  definition:
    definition: "The eleventh level of perception in Powers' (1998) hierarchy controls
      combinations of principle level perceptions as a single unit, such that it forms
      a coherent organization of principles.  \n\nSystem concept perceptions contain
      for instance perceptions of the kind of person we are or (an aspect of) our
      worldview. At this level, all principles combine into a single unit a coherent
      organization of principles. Control at this level is experienced like something
      is true, it fits your worldview (De Hullu, 2023).  \n\nThese high level perceptions
      do not change easily. A person that has a system concept of themselves as a
      certain sports team fan, rarely becomes a fan of another team or no sportsfan
      at all. Powers (1998) explains that the system concepts that have the most influence
      on our lives are those in which we believe, which we see as the truth and reality.
      He also explains that we will exert a lot of effort to be able to maintain those
      important system concepts. This means that that can be the cause of trouble
      when there are very different system concepts between people.  \n"
  measure_dev:
    instruction: 'System concept perceptions can be measured in an open questioning
      format. Before formulating questions regarding system concepts, it must be determined
      what theme or types of system concepts are to be researched. We can present
      a certain statement and ask questions such as: ’Does this sound true to you
      or false?''. '
  measure_code:
    instruction: System concept perceptions are measured with questions in a questionnaire.
      Items can be coded as measuring system concepts, when it asks about whether
      a person believes something to be true (reality), or false.
  aspect_dev:
    instruction: "Conduct a qualitive study, where participants are individually interviewed
      in a free-response format. The interviews are recorded and transcribed, or notes
      are taken. The transcript or notes are coded as 'for aspect coding'.  \n\nIn
      the interview, ask participants to describe what their understanding of a certain
      concept entails. Ask not for details, but for the higher level meaning. ‘What
      makes [target system concept] true for you?’   \n\nAnother approach would be
      to challenge one’s system concept, by providing statements to rate as true or
      false. This builds on the idea of disturbances to system concept control. \n\nFor
      example, providing a statement such as: \n‘A stimulus elicits a response.’ \nWill
      be rated as true for people who see behaviour according to the behaviourist
      framework, and as false for PCT people.  \nRatings should also have the option
      to be uncertain. That would mean that this statement does not relate to system
      concepts for people (who do not hold strong opinions on stimuli and responses).
      \ \n\nPossible response options would be:  \nThis is true and I am very certain
      \nThis is true and I am not very certain \nThis may be true or false \nThis
      is false and I am not very certain \nThis is false and I am very certain \n"
  aspect_code:
    instruction: "Expressions about system concepts, about what principles mean to
      you, about your identity, personality and worldview.  \nExamples are: 'Being
      a good parent (system concept) means giving your child some autonomy (principle)
      and protecting it (principle)'.\n"
  comments: ''
  rel: ~


