---
title: "Deploying a CoRe"
---

## Deploying a CoRe

This page describes how you can adapt and deploy your own construct repository (CoRe).

This repository is open source. It's a static site created with Quarto; you can clone it from [https://codeberg.org/explicate/PsyCoRe.one](https://codeberg.org/explicate/PsyCoRe.one) and then adjust it to your needs. This page highlights the parts of the site that handle the repository bits.

This page assumes you're familiar with the concept of the repository. If not, please first read "[Knowing What We're Talking About: Facilitating Decentralized, Unequivocal Publication of and Reference to Psychological Construct Definitions and Instructions](https://doi.org/mr4n)".

### The data

The data are located in a directory called "`data`" and then subdirectory "`dctSpecs`". These are the YAML files with the Decentralized Construct Taxonomy specifications.

### The construct overview

The construct overview is located in directory "`overview`". That directory contains two files: `index.qmd` and `psycore-overview-template.ejs`. When rendering the website, Quarto processed `index.qmd` to produce `index.html`, the view showing the overview of constructs; and in doing that, employing the [Embedded Javascript Template](https://ejs.co/) in `psycore-overview-template.ejs`.

Quarto knows to use that template because it is specified in the YAML preamble to `index.qmd`. This specifies a "listing" (see [https://quarto.org/docs/websites/website-listings-custom.html](https://quarto.org/docs/websites/website-listings-custom.html)): here, Quarto loops over elements specified in the "`contents`", formats the output using the template, and then inserts the result in the `<div>` element with identifier `id`, as specified here:

```YAML
---
title: "Construct overview"
listing:
  - id: "construct-overview"
    template: "psycore-overview-template.ejs"
    contents: "../data/dctSpecs/*.yaml"
    sort-ui: false
    filter-ui: false
    page-size: 999
    fields: [label, dctId]
    field-display-names:
      label: "Construct Label"
      dct.label: "Construct Label"
      dctId: "UCID"
      dct: "Test"
---
```

The actual contents of `index.qmd` are just that `<div>` with that identifier:

```
::: {#construct-overview}
:::
```

### Single construct page

The single construct page is located in directory "`construct`". That directory also contains two files: `index.qmd` and `psycore-construct-template.ejs`. In this case, `index.qmd` contains one additional element next to the `<div>` where the results of looping over the YAML files using the embedded javascript template is inserted:

```html
---
title: "Single Construct View"
listing:
  - id: "construct"
    template: "psycore-construct-template.ejs"
    contents: "../data/dctSpecs/*.yaml"
    sort-ui: false
    filter-ui: false
    page-size: 9999
---

::: {#construct}
:::

<script defer src="../js/construct.js">
</script>
```

That extra element loads the javascript file `construct.js` in the `js` directory, a "sibling" to the `construct` directory. That javascript file contains the following:

```javascript
document.addEventListener("DOMContentLoaded", () => {
  
  var urlParams = new URLSearchParams(window.location.search);
  var ucid = urlParams.get('ucid');
  
  document.getElementById(ucid+"_view").style.display = "block";
  
});
```

What it does is wait until all DOM content loaded; then retrieve the contents of the GET parameter named `ucid`; and the set the `display` CSS property of the element with identifier equal to that UCID appended with "`_view`" to "`display`".

Because the javascript template sets this property to "`none`" for all constructs, the page will not show any constructs when it loads. As a result, the page will appear to be a dedicated page for one construct (that specified in the `ucid` GET parameter).

### The `.htaccess` file

"Out of the box", the "single construct pages" have a URL such as "`[https://psycore.one/construct/?ucid=attitude_73dnt5zc](https://psycore.one/construct/?ucid=attitude_73dnt5zc)`". To make sure that the pretty short URLs to uniquely identify constructs (like "`[https://psycore.one/attitude_73dnt5zc](https://psycore.one/attitude_73dnt5zc)`") work, some URL rewriting has to be done.

The Apache web server can do this with a `.htaccess` file. This should contain the following lines:

```
RewriteEngine on

RewriteCond %{HTTP_HOST} ^psycore\.one$ [OR]
RewriteCond %{HTTP_HOST} ^www\.psycore\.one$
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule ^(.*)$ /construct?ucid=$1 [R=302,L]
```

Of these, the first specifies that URL rewriting should be activated. The two `RewriteCond %{HTTP_HOST}` lines specify that the following rewriting directive should only be executed for requests for the domain name `psycore.one` (`[OR] www.psycore.one`). Then, `RewriteCond %{REQUEST_FILENAME} !-f` specifies the condition that the following rewrite directive should only be executed if the request refers to a file that does not exist on the server; and `RewriteCond %{REQUEST_FILENAME} !-d` specifies the condition that the following rewrite directive should only be executed if the request refers to a non-existing directory. Finally, the actual rewrite directive `RewriteRule ^(.*)$ /construct?ucid=$1 [R=302,L]` rewrites the URL by taking everything after the domain name (captured by regular expression `^(.*)$`) and inserting it after `/construct?ucid=` (following the domain name). Finally, the three flags `[NC,L,R=302]` specify that the regular expression is not case sensitive (the `NC`), that this should be the last rewriting directive the server executes (the `L`) and that this redirect should be communicated to the browser as a temporary redirect (`R=302`).

### The Embedded Javascript Templates

The [full EJS documentation](https://ejs.co/#docs) is available [here](https://ejs.co/#docs).

In EJS files, the templating functionality is contained in between HTML-like tags: `<%` and `%>`, where the first (opening) tag is often followed by a modifier. A simnple example is:

```
    <% for (const item of items) { %>
```

The above line initiates a loop through all `items`, where in each iteration of the loop, the current item can be accessed with `item`.

In PsyCoRe, these items are the DCT specifications. The values in the current DCT specification can be accessed with periods. For example, the identifier of a construct can be accessed with `<%= item.dct.id %>`. The equals sign modifies the tag to indicate that we want to output the value after HTML escaping it. The following fragment inserts a paragraph containing the construct's definition:

```
<p><%= item.dct.definition.definition %></p>
```

The double "definition" follows the structure of the DCT YAML specification:

```
dct:
  version: 0.1.0
  id: someConstruct_79n2w1bh
  label: The construct's label
  definition:
    definition: |-
      The definiton of the construct goes here.
```

Because the standard also allows specifying other information alongside the definiton, such as its source(s), the `definition` itself has its own field name within the `definition` container.
